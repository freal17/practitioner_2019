var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();
var totalUsers = 0;
var logeados = [];
var unloggeds = [];
const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3001;

// parse application/json
app.use(bodyParser.json());

//Petición GET de todos los 'users'
app.get(URL_BASE + 'users',
    function (request, response) {
        console.log('GET ' + URL_BASE + 'users');
        response.send(userFile);
    }
);

//Petición GET de todos los 'users' (INSTANCE)
app.get(URL_BASE + 'users/:id',
    function (request, response) {
        console.log('GET ' + URL_BASE + 'users/id');
        console.log(request.params.id);
        let indice = request.params.id;
        let instancia = { "mensaje": "Recurso no encontrado." };
        userFile.forEach(e => {
            if (e.idUsuario == indice) {
                instancia = e;
            }
        });

        response.status(200);

        console.log('Instancia: ');
        console.log(instancia);
        console.log('Query: ');
        console.log(request.query);

        response.send(instancia);
    }
);

//Petición GET con Query String
app.get(URL_BASE + 'usersq',
    function (request, response) {
        console.log('GET ' + URL_BASE + 'con query string');
        console.log(request.query);
        //response.send({'mensaje':'éxito'});
        response.send(request.query);
    }
);

//Petición POST
app.post(URL_BASE + 'userspost',
    function (request, response) {
        totalUsers = userFile.length;
        let respuesta = { "mensaje": "No existen datos en el body" };
        if (request.body != undefined && Object.keys(request.body).length > 0) {
            let newUser = {
                idUsuario: totalUsers + 1,
                first_name: request.body.first_name,
                last_name: request.body.last_name,
                email: request.body.email,
                password: request.body.password
            };
            userFile.push(newUser);
            response.status(200);
            respuesta = {
                "mensaje": "Usuario creado con éxito",
                "usuario": newUser,
                "nro_usuarios": userFile.length
            };
        }
        console.log(respuesta);
        response.send(respuesta);
    }
);

//Petición PUT
app.put(URL_BASE + 'usersput',
    function (request, response) {
        totalUsers = userFile.length;
        let respuesta = { "mensaje": "Usuario no existe" };
        if (request.body != undefined && Object.keys(request.body).length > 0) {
            userFile.forEach(e => {
                if (e.idUsuario == request.body.idUsuario) {
                    e.first_name = request.body.first_name,
                        e.last_name = request.body.last_name,
                        e.email = request.body.email,
                        e.password = request.body.password
                    respuesta = {
                        "mensaje": "Usuario actualizado con éxito",
                        "nro_usuarios": userFile.length,
                        "usuarios": userFile
                    };
                }
            });

        }
        console.log(respuesta);
        response.send(respuesta);
    }
);

//Petición DELETE
app.delete(URL_BASE + 'usersdelete',
    function (request, response) {
        totalUsers = userFile.length;
        let respuesta = { "mensaje": "Usuario no existe" };
        if (request.body != undefined && Object.keys(request.body).length > 0) {
            userFile.forEach(e => {
                if (e.idUsuario == request.body.idUsuario) {
                    userFile.splice(userFile.indexOf(e), 1);
                    respuesta = {
                        "mensaje": "Usuario eliminado con éxito",
                        "nro_usuarios": userFile.length,
                        "usuarios": userFile
                    };
                }
            });

        }
        response.send(respuesta);
    }
);

//LOGIN
app.post(URL_BASE + 'login',
    function (request, response) {
        let body = request.body;
        var email = body.email;
        var password = body.password;
        var rpta = { 'mensaje': 'Login incorrecto' };
        console.log(email);
        console.log(password);
        userFile.forEach(user => {
            if (user.email == email && user.password == password) {
                if (user.logged) {
                    rpta = { 'mensaje': 'Login incorrecto, el usuario ya se encuentra logeado', 'usuario': user.idUsuario };
                } else {
                    user.logged = true;
                    rpta = { 'mensaje': 'Login correcto', 'usuario': user.idUsuario };
                    updateUserFile(userFile);
                }
            }
        });
        response.send(rpta);
    }
);

//LOGOUT
app.post(URL_BASE + 'logout/:id',
    function (request, response) {
        var id = request.params.id;
        var rpta = { 'mensaje': 'Logout incorrecto' };
        userFile.forEach(user => {
            if (user.idUsuario == id && Boolean(user.logged)) {
                delete user.logged;
                rpta = { 'mensaje': 'Logout correcto', 'usuario': id };
                updateUserFile(userFile);
            }
        });

        response.send(rpta);
    }
);


//GET LOGEADOS
app.get(URL_BASE + 'loggeds',
    function (request, response) {
        let init = request.query.init;
        let fin = request.query.fin;
        let rpta = { 'mensaje': 'No existen datos' };
        logeados = [];
        userFile.forEach(user => {
            if (user.logged) {
                logeados.push(user);
            }
        });

        if (logeados != '') {
            rpta = logeados;
            if (init != undefined && fin != undefined &&
                Number(init) != isNaN && Number(fin) != isNaN &&
                init < fin) {
                rpta = logeados.slice(init == 1 ? 0 : init, fin);
            }
        }


        response.send(rpta);
    }
);

//GET UNLOGGEDS
app.get(URL_BASE + 'unloggeds',
    function (request, response) {
        let rpta = { 'mensaje': 'No existen datos' };
        unloggeds = [];
        userFile.forEach(user => {
            if (!user.logged) {
                unloggeds.push(user);
            }
            rpta = unloggeds;
        });

        response.send(rpta);
    }
);

/* Masivos para pruebas */
//LOGIN Masivo
app.post(URL_BASE + 'loginmasivo',
    function (request, response) {
        let masivo = request.body.masivo;
        var rpta = { 'mensaje': 'Login incorrecto' };
        console.log(masivo);
        masivo.forEach(login => {
            userFile.forEach(user => {
                if (user.email == login.email && user.password == login.password) {
                    if (user.logged) {
                        rpta = { 'mensaje': 'Login incorrecto, el usuario ya se encuentra logeado', 'usuario': user.idUsuario };
                    } else {
                        user.logged = true;
                        rpta = { 'mensaje': 'Login correcto', 'usuario': user.idUsuario };
                        updateUserFile(userFile);
                    }
                }
            });
        });
        response.send(rpta);
    }
);

//Logout Masivo
app.post(URL_BASE + 'logoutmasivo',
    function (request, response) {
        let masivo = request.body.masivo;
        let rpta = { 'mensaje': 'Login incorrecto' };
        masivo.forEach(login => {
            userFile.forEach(user => {
                if (user.idUsuario == login.idUsuario && user.logged) {
                    delete user.logged;
                    rpta = { 'mensaje': 'Login masivo correcto' };
                    updateUserFile(userFile);
                }
            });
        });
        response.send(rpta);
    }
);


function updateUserFile(user) {
    var jsonUserData = JSON.stringify(user);
    fs.writeFile("./user.json", jsonUserData, "utf8",
        function (e) {
            if (e) {
                console.log(e);
            } else {
                console.log("Datos escritos en 'user.json'.");
            }
        });
}

app.listen(PORT,
    function () {
        console.log('API-TECHU escuchando en puerto ' + PORT + '...');
    }
);